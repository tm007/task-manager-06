package ru.tsc.apozdnov.tm;

import ru.tsc.apozdnov.tm.constant.ArgumentConstant;
import ru.tsc.apozdnov.tm.constant.TerminalConstant;

import java.util.Scanner;

public final class Application {

    public static void main(final String[] args) {
        if (processArgumentTask(args)) System.exit(0);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String cmd = scanner.nextLine();
            processCommandTask(cmd);
        }
    }

    public static void processCommandTask(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConstant.ABOUT:
                showAbout();
                break;
            case TerminalConstant.VERSION:
                showVersion();
                break;
            case TerminalConstant.HELP:
                showHelp();
                break;
            case TerminalConstant.EXIT:
                close();
                break;
            default:
                showFaultCommnand(command);
                break;
        }
    }

    public static void processArgumentTask(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            default:
                showFaultArgument(arg);
                break;
        }
    }

    public static boolean processArgumentTask(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgumentTask(arg);
        return true;
    }

    public static void close() {
        System.exit(0);
    }

    public static void showWelcome() {
        System.out.println("**** Welcome to Task Manager ****");
    }

    public static void showFaultArgument(String fault) {
        System.err.printf("Fault... This argument `%s` not supported...  \n", fault);
    }

    public static void showFaultCommnand(String fault) {
        System.err.printf("Fault... This command `%s` not supported...  \n", fault);
    }

    public static void showAbout() {
        System.out.println("[About]");
        System.out.println("[Name: Aleksandr Pozdnov]");
        System.out.println("[E-mail: apozdnov@t1.com]");
    }

    public static void showVersion() {
        System.out.println("1.6.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.format("%s, %s - Show developer info.\n", TerminalConstant.ABOUT, ArgumentConstant.ABOUT);
        System.out.format("%s, %s - Show application version.\n", TerminalConstant.VERSION, ArgumentConstant.VERSION);
        System.out.format("%s, %s - Show terminal commands.\n", TerminalConstant.HELP, ArgumentConstant.HELP);
        System.out.format("%s - Close application Task Manager.\n", TerminalConstant.EXIT);
    }

}
